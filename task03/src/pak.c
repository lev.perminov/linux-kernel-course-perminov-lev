#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 0xFF - maximum value for binary data series processing in a simple one-byte implementation.
#define MAX_INPUT_MESSAGE 256

int main(int argc, char *argv[])
{
	char inputMessage[MAX_INPUT_MESSAGE] = {'\0',};
	char *packedMessage = NULL;
	char ch;
	unsigned int cnt = 1;
	unsigned int offset = 0;
	size_t length;

	if(!fgets(inputMessage, MAX_INPUT_MESSAGE, stdin))
		return 0;

	ch = inputMessage[0];

	// In the worst case (when there is no character series) the packed message will be double size of incoming message
	length = strlen(inputMessage);
	if ( (packedMessage = (char*)calloc(length*2 + 1, sizeof(char))) )
	{
		for (int i = 1; i <= length; i++) {
			if (inputMessage[i]==ch) {
				cnt++;
			}
			else {
				sprintf(packedMessage+offset, "%c%c", cnt, ch);
				ch = inputMessage[i];
				cnt = 1;
				offset += 2;
			}
		}
	}

	printf("%s", packedMessage);
	free(packedMessage);
}
