#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_INPUT_MESSAGE 256

#define DIV_ROUND_UP(a,b) (((a) + (b) - 1) / (b))
#define REALLOC_STEP (MAX_INPUT_MESSAGE*((DIV_ROUND_UP(offset,MAX_INPUT_MESSAGE))+1))

int main(int argc, char *argv[])
{
	char inputMessage[MAX_INPUT_MESSAGE] = {'\0',};
	char *unpackedMessage = NULL;
	char *p = NULL;
	unsigned int cnt = 0;
	unsigned int offset = 0;
	size_t length;

	if(!fgets(inputMessage, MAX_INPUT_MESSAGE, stdin))
		return 0;

	length = strlen(inputMessage);
	if ((unpackedMessage = (char*)calloc(MAX_INPUT_MESSAGE, sizeof(char))))
	{
		for (int i = 0; i < length; i++) {
			if (i%2 == 0) {
				cnt = (unsigned char)inputMessage[i];
			}
			else {
				while (cnt--){
					sprintf(unpackedMessage+(offset++), "%c", inputMessage[i]);
					if (offset % (MAX_INPUT_MESSAGE) == 255){
						if ((p = realloc(unpackedMessage, sizeof(char)*REALLOC_STEP)) ){
							unpackedMessage = p;
						}
						else{
							free(unpackedMessage);
							return -1;
						}
					}
				}
			}
		}
	}

	printf("%s", unpackedMessage);
	free(unpackedMessage);
}
